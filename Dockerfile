FROM openjdk:8
ADD target/docker-hometest.jar docker-hometest.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "docker-hometest.jar"]
	