package com.hometest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.hometest.model.User;
import com.hometest.service.UserService;
import com.hometest.service.UserServiceImpl;

@SpringBootTest
class HomeTestApplicationTests {
	
	
	@MockBean
	private UserServiceImpl service;
	
	@Autowired
	private UserService userinterface;
	

	@Test
	public void getAllUserTest() {
		when(userinterface.getAllUser()).thenReturn(Stream 
				.of(new User(101,102,"title101", "Body01"), new User(201,202,"title201", "Body202")).collect(Collectors.toList()));
		assertEquals(2,service.getAllUser().size());
	}
	
	@Test
	public void getAllUsersUniqueTest() {
		when(userinterface.getAllUsersUnique()).thenReturn(Stream 
				.of(new User(101,102,"title101", "Body01"), new User(201,202,"title201", "Body202")).collect(Collectors.toList()));
		assertEquals(2,service.getAllUsersUnique().size());
	}


	
	@Test
	public void getUserByIdSTest() {
		int id = 201;
		User user = new User(201,202,"title201", "Body201");
				when(userinterface.getUserById(id)).thenReturn(user);
		assertEquals(user,service.getUserById(id));
					
	}
	
	@Test
	public void addUserTest() {
		User user = new User(301,301,"title301", "Body301");
				when(userinterface.addUser(user)).thenReturn(user);
		assertEquals(user,service.addUser(user));
					
	}
	
	@Test
	public void updateUserTest() {
		
		User user = new User(1, 4,"1800 flowers", "BodyTest");
				userinterface.updateUser(user);
				verify(userinterface,times(1)).updateUser(user);
	}
	
	@Test
	public void deleteUserTest() {
		int id = 201;
				userinterface.deleteUser(id);
				verify(userinterface,times(1)).deleteUser(id);
	}
	

}
