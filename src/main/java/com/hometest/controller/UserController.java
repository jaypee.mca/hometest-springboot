package com.hometest.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.hometest.model.*;
import com.hometest.service.UserServiceImpl;

@RestController
public class UserController {
	
	@Autowired
	private UserServiceImpl userService;
	
	@GetMapping("/count/")
	public Map<String,Integer> Count() {
		return userService.getCount();
	}
	
	@GetMapping("/usercount/")
	public Map<String,Integer> UserCount() {
		return userService.updatedUserCount();
	}
	
	@GetMapping("/updatedUserList/")
	public List<User> UserListUpdatedwithUniqueValues() {
		return userService.getAllUsersUnique();
	}
	
	
	@GetMapping("/user/")
	public List<User> getAllUser(){
		return userService.getAllUser();
		
	}
	@GetMapping("/user/{userId}")
	public User getUserById(@PathVariable int userId) {
		return userService.getUserById(userId);
		
	}
	
	@PostMapping("/user/")
	 public ResponseEntity<Void> addUser(@RequestBody User newUser, UriComponentsBuilder builder){
	  User user = userService.addUser(newUser);
	  
	  if(user == null) {
	   return ResponseEntity.noContent().build();
	  }
	  
	  HttpHeaders headers = new HttpHeaders();
	  headers.setLocation(builder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
	  return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	 }
	
	@PutMapping("/user/")
	 public ResponseEntity<User> updateUser(@RequestBody User user){
	  User u = userService.getUserById(user.getId());
	  
	  if(u == null) {
	   return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	  }
	  
	  u.setUserId(user.getUserId());
	  u.setBody(user.getBody());
	  u.setTitle(user.getTitle());
	
	  
	  //userService.updateUser(u);
	  return new ResponseEntity<User>(u, HttpStatus.OK);
	 }
	
	@DeleteMapping("/user/{userId}")
	 public ResponseEntity<User> deleteUser(@PathVariable int userId){
	  User u = userService.getUserById(userId);
	  
	  if(u == null) {
	   return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	  }
	  
	//  userService.deleteUser(userId);
	  return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	 }
	


}
