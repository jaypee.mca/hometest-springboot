package com.hometest.service;

import java.util.List;
import java.util.Map;

import com.hometest.model.*;

public interface UserService {
	
	public List<User> getAllUser();
	
	public User getUserById(int id);
	public User addUser(User user);
	public void updateUser(User user);
	public void deleteUser(int id);
	
	public Map<String,Integer>  getCount();

	public List<User> getAllUsersUnique();

	public Map<String,Integer>  updatedUserCount();
	
	
	
	
	
	
	

}
