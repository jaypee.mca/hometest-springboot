package com.hometest.service;

import java.io.IOException;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Random;
import java.util.TreeSet;



import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hometest.model.*;

@Component
public class UserServiceImpl implements UserService{
	
	
	public  static List<User> users = new ArrayList<>(); 
	public static int userCount;
	static {
		
		try {
		
			 RestTemplate restTemplate = new RestTemplate();
			 String response = restTemplate.getForObject("http://jsonplaceholder.typicode.com/posts", String.class);
			 
			 ObjectMapper objectMapper = new ObjectMapper();
		 
			 List<User> usersInfo = objectMapper.readValue(response,objectMapper.getTypeFactory().constructCollectionType(List.class, User.class));
			 users = usersInfo;
			 userCount = users.size();
			 
			 
		}
		catch (IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public  Map<String,Integer> getCount() {
		List<User> unique = users.stream()
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingInt(User::getId))),
                                           ArrayList::new));
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("Count", unique.size());
		
		return map;
		
	}
	
	@Override	
	public List<User> getAllUsersUnique() {
		// TODO Auto-generated method stub
		
		List<User> unique = users.stream()
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingInt(User::getUserId))),
                                           ArrayList::new));
		
		return unique;
	}
	 

	
	public  Map<String,Integer> updatedUserCount() {

		List<User> unique = users.stream()
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingInt(User::getUserId))),
                                           ArrayList::new));
		
		
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("UserCount", unique.size());
		
		return map;
		
	}
	
	@Override	
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		
		System.out.println("mockuptest: " + users);
		
		return users;
	}

	@Override
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		
		for(User user: users) {
			if(user.getId() == id) {
				return user;
			}
		}
		return null;
	}

	@Override	
	public User addUser(User user) {
		// TODO Auto-generated method stub
		
		Random random = new Random();
		int nextId = random.nextInt(1000) + 10;
		
		user.setId(nextId);
		users.add(user);
		return user;

	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		for(User oldUser: users) {
			if(oldUser.getId() == user.getId()) {
				oldUser.setUserId(user.getUserId());
				oldUser.setTitle(user.getTitle());
				oldUser.setBody(user.getBody());
				
			}
		}
		
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		for(User u: users) {
			if(u.getId() == id) {
				users.remove(u);
				break;
			}
			
		}
		
	}
	
	
	
	
	
	

}
